package com.android.godot;

import android.app.Activity;
import android.util.Log;
import android.widget.Toast;

//import AdBuddiz SDK
import com.purplebrain.adbuddiz.sdk.AdBuddiz;
import com.purplebrain.adbuddiz.sdk.AdBuddizDelegate;
import com.purplebrain.adbuddiz.sdk.AdBuddizError;
import com.purplebrain.adbuddiz.sdk.AdBuddizLogLevel;

/**
 * Created by alexey on 27.03.15.
 */

public class GodotAdBuddiz extends Godot.SingletonBase {
    private static final String TAG = "AdBuddiz";
    private Activity activity = null;
    private boolean active = false;

    public GodotAdBuddiz(Activity activity) {
        registerClass("AdBuddiz", new String[]
        {
            "init", "showAd"
        });

        this.activity = activity;
    }

    static public Godot.SingletonBase initialize(Activity p_activity)
    {
        return new GodotAdBuddiz(p_activity);
    }

    public void init(boolean test, String publisherKey) {
        if(active)
            return;

        AdBuddiz.setLogLevel(AdBuddizLogLevel.Info);    // log level
        AdBuddiz.setPublisherKey(publisherKey);         // replace with your app publisher key

        if (test) {
            AdBuddiz.setTestModeActive();               // to delete before submitting to store
            AdBuddiz.setDelegate(delegate);
        }

        AdBuddiz.cacheAds(activity);                    // start caching ads

        active = true;
    }

    public void showAd() {
        if (active) {
            AdBuddiz.showAd(activity);                    // start caching ads
            //Toast.makeText(activity, "on show", Toast.LENGTH_SHORT).show();
        }
    }

    protected void onMainDestroy() {
        if (active) {
            //Toast.makeText(activity, "onMainDestroy", Toast.LENGTH_SHORT).show();
            AdBuddiz.onDestroy(); // to minimize memory footprint
            active = false;
        }
    }

    private AdBuddizDelegate delegate = new AdBuddizDelegate()  {
        @Override
        public void didCacheAd() {
            Log.i(TAG, "didCacheAd");
            Toast.makeText(activity, "didCacheAd", Toast.LENGTH_SHORT).show();
        }

        @Override
        public void didShowAd() {
            Log.i(TAG, "didShowAd");
            Toast.makeText(activity, "didShowAd", Toast.LENGTH_SHORT).show();
        }

        @Override
        public void didFailToShowAd(AdBuddizError error) {
            String msg = "FailToShow: " + error.name();
            Log.i(TAG, msg);
            Toast.makeText(activity, msg, Toast.LENGTH_SHORT).show();
        }

        @Override
        public void didClick() {
            Log.i(TAG, "didClick");
            Toast.makeText(activity, "didClick", Toast.LENGTH_SHORT).show();
        }

        @Override
        public void didHideAd() {
            Log.i(TAG, "didHideAd");
            Toast.makeText(activity, "didHideAd", Toast.LENGTH_SHORT).show();
        }
    };

}
