extends Node

export var test_mode = false
export var public_key = "key"

var adbuddiz = null

func _ready():
	if(Globals.has_singleton("AdBuddiz")):
		adbuddiz = Globals.get_singleton("AdBuddiz")
		adbuddiz.init(test_mode, public_key)


func _on_show_released():
	if adbuddiz != null:
		adbuddiz.showAd()
